//
//  CalculatorViewModel.swift
//  Schultopf
//
//  Created by Stefan Kofler on 06.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol CalculatorViewModel {
    // Inputs
    var summandOne: BehaviorSubject<Int> { get }
    var summandTwo: BehaviorSubject<Int> { get }

    // Outputs
    var sum: Observable<Int> { get }
}

class CalculatorViewModelImpl: CalculatorViewModel {
    // Inputs
    let summandOne = BehaviorSubject(value: 0)
    let summandTwo = BehaviorSubject(value: 0)
    let price = BehaviorSubject(value: 0)

    // Outputs
    lazy var sum = Observable.combineLatest(summandOne, summandTwo) { $0 + $1 }

    lazy var priceString = price.map { "\($0) Euro" }

}
