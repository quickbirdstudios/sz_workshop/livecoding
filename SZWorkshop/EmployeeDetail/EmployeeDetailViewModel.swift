//  
//  EmployeeDetailViewModel.swift
//  SZWorkshop
//
//  Created by Stefan Kofler on 06.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation
import RxSwift

protocol EmployeeDetailViewModel {
    // Output
    var title: Observable<String> { get }
}

class EmployeeDetailViewModelImpl: EmployeeDetailViewModel {

    private let employee: String

    init(employee: String) {
        self.employee = employee
    }

    // Output

    lazy var title = Observable.just(employee)

}
