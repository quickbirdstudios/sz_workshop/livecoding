//
//  EmployeeService.swift
//  SZWorkshop
//
//  Created by Stefan Kofler on 09.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation
import RxSwift

protocol EmployeeService {
    func getEmployees() -> Observable<[String]>
}

class EmployeeServiceImpl: EmployeeService {
    func getEmployees() -> Observable<[String]> {
        return Observable.just(["Stefan", "Malte", "Paul", "Julian", "Sebastian", "Quirin", "Joan"])
    }
}
